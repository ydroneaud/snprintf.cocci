
#include <stdio.h>

#define SNPRINTF(arr, fmt, ...) snprintf(arr, sizeof(arr), fmt, ## __VA_ARGS__)

/* "2\0" */
#define LENGTH2 2

/* "33\0" */
#define LENGTH3 3

char global_string_var[2];

// anon structure variable
struct {
	char string1[2];
	char string2[LENGTH3];
} global_anon_struct_var;

// named structure variable and pointer
struct named_struct {
	char string1[2];
	char string2[LENGTH3];
};

struct named_struct  global_named_struct_var;
struct named_struct *global_named_struct_ptr = &global_named_struct_var;

// named structure type variable and pointer
typedef struct named_struct named_struct_t;

named_struct_t  global_named_struct_t_var;
named_struct_t *global_named_struct_t_ptr = &global_named_struct_t_var;

// named structure type variable and pointer
typedef struct named_struct2
{
	char string1[2];
	char string2[LENGTH3];
} named_struct2_t;

named_struct2_t  global_named_struct2_t_var;
named_struct2_t *global_named_struct2_t_ptr = &global_named_struct2_t_var;

// anonymous structure type variable and pointer
typedef struct {
	char string1[2];
	char string2[LENGTH3];
} anon_struct_t;

anon_struct_t  global_anon_struct_t_var;
anon_struct_t *global_anon_struct_t_ptr = &global_anon_struct_t_var;

// a structure that contains other structures
struct container {
	struct named_struct named_member;
	named_struct_t      named_t_member;
	anon_struct_t       anon_t_member;
};

struct container  global_container_var;
struct container *global_container_ptr = &global_container_var;

void global_test(void)
{
	// global string
	snprintf(global_string_var, 2, "2");
	snprintf(global_string_var, sizeof(global_string_var), "2");

	// global anon struct variable
	snprintf(global_anon_struct_var.string1, 2, "2");
	snprintf(global_anon_struct_var.string1, sizeof(global_anon_struct_var.string1), "2");
	snprintf(global_anon_struct_var.string2, 3, "33"); // no expectation
	snprintf(global_anon_struct_var.string2, LENGTH3, "33");
	snprintf(global_anon_struct_var.string2, sizeof(global_anon_struct_var.string2), "33");

	// global named struct variable
	snprintf(global_named_struct_var.string1, 2, "2");
	snprintf(global_named_struct_var.string1, sizeof(global_named_struct_var.string1), "2");
	snprintf(global_named_struct_var.string2, 3, "33"); // no expectation
	snprintf(global_named_struct_var.string2, LENGTH3, "33");
	snprintf(global_named_struct_var.string2, sizeof(global_named_struct_var.string2), "33");

	// global named struct pointer
	snprintf(global_named_struct_ptr->string1, 2, "2");
	snprintf(global_named_struct_ptr->string1, sizeof(global_named_struct_ptr->string1), "2");
	snprintf(global_named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(global_named_struct_ptr->string2, LENGTH3, "33");
	snprintf(global_named_struct_ptr->string2, sizeof(global_named_struct_ptr->string2), "33");

	// global named struct type variable (with existing struct)
	snprintf(global_named_struct_t_var.string1, 2, "2");
	snprintf(global_named_struct_t_var.string1, sizeof(global_named_struct_t_var.string1), "2");
	snprintf(global_named_struct_t_var.string2, 3, "33"); // no expectation
	snprintf(global_named_struct_t_var.string2, LENGTH3, "33");
	snprintf(global_named_struct_t_var.string2, sizeof(global_named_struct_t_var.string2), "33");

	// global named struct type pointer (with existing struct)
	snprintf(global_named_struct_t_ptr->string1, 2, "2");
	snprintf(global_named_struct_t_ptr->string1, sizeof(global_named_struct_t_ptr->string1), "2");
	snprintf(global_named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(global_named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(global_named_struct_t_ptr->string2, sizeof(global_named_struct_t_ptr->string2), "33");

	// global named struct type variable
	snprintf(global_named_struct2_t_var.string1, 2, "2");
	snprintf(global_named_struct2_t_var.string1, sizeof(global_named_struct2_t_var.string1), "2");
	snprintf(global_named_struct2_t_var.string2, 3, "33"); // no expectation
	snprintf(global_named_struct2_t_var.string2, LENGTH3, "33");
	snprintf(global_named_struct2_t_var.string2, sizeof(global_named_struct2_t_var.string2), "33");

	// global named struct type pointer
	snprintf(global_named_struct2_t_ptr->string1, 2, "2");
	snprintf(global_named_struct2_t_ptr->string1, sizeof(global_named_struct2_t_ptr->string1), "2");
	snprintf(global_named_struct2_t_ptr->string2, 3, "33"); // no expectation
	snprintf(global_named_struct2_t_ptr->string2, LENGTH3, "33");
	snprintf(global_named_struct2_t_ptr->string2, sizeof(global_named_struct2_t_ptr->string2), "33");

	// global anonymous struct type variable
	snprintf(global_anon_struct_t_var.string1, 2, "2");
	snprintf(global_anon_struct_t_var.string1, sizeof(global_anon_struct_t_var.string1), "2");
	snprintf(global_anon_struct_t_var.string2, 3, "33"); // no expectation
	snprintf(global_anon_struct_t_var.string2, LENGTH3, "33");
	snprintf(global_anon_struct_t_var.string2, sizeof(global_anon_struct_t_var.string2), "33");

	// global anonymous struct type pointer
	snprintf(global_anon_struct_t_ptr->string1, 2, "2");
	snprintf(global_anon_struct_t_ptr->string1, sizeof(global_anon_struct_t_ptr->string1), "2");
	snprintf(global_anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(global_anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(global_anon_struct_t_ptr->string2, sizeof(global_anon_struct_t_ptr->string2), "33");

	// global container variable
	struct named_struct *named_struct_ptr;

	named_struct_ptr = &global_container_var.named_member;

	snprintf(named_struct_ptr->string1, 2, "2");
	snprintf(named_struct_ptr->string1, sizeof(named_struct_ptr->string1), "2");
	snprintf(named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_ptr->string2, sizeof(named_struct_ptr->string2), "33");

	named_struct_ptr = &global_container_ptr->named_member;

	snprintf(named_struct_ptr->string1, 2, "2");
	snprintf(named_struct_ptr->string1, sizeof(named_struct_ptr->string1), "2");
	snprintf(named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_ptr->string2, sizeof(named_struct_ptr->string2), "33");

	named_struct_t *named_struct_t_ptr;

	named_struct_t_ptr = &global_container_var.named_t_member;

	snprintf(named_struct_t_ptr->string1, 2, "2");
	snprintf(named_struct_t_ptr->string1, sizeof(named_struct_t_ptr->string1), "2");
	snprintf(named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_t_ptr->string2, sizeof(named_struct_t_ptr->string2), "33");

	named_struct_t_ptr = &global_container_ptr->named_t_member;

	snprintf(named_struct_t_ptr->string1, 2, "2");
	snprintf(named_struct_t_ptr->string1, sizeof(named_struct_t_ptr->string1), "2");
	snprintf(named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_t_ptr->string2, sizeof(named_struct_t_ptr->string2), "33");

	anon_struct_t *anon_struct_t_ptr;

	anon_struct_t_ptr = &global_container_var.anon_t_member;

	snprintf(anon_struct_t_ptr->string1, 2, "2");
	snprintf(anon_struct_t_ptr->string1, sizeof(anon_struct_t_ptr->string1), "2");
	snprintf(anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(anon_struct_t_ptr->string2, sizeof(anon_struct_t_ptr->string2), "33");

	anon_struct_t_ptr = &global_container_ptr->anon_t_member;

	snprintf(anon_struct_t_ptr->string1, 2, "2");
	snprintf(anon_struct_t_ptr->string1, sizeof(anon_struct_t_ptr->string1), "2");
	snprintf(anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(anon_struct_t_ptr->string2, sizeof(anon_struct_t_ptr->string2), "33");
}

void local_test(void)
{
	// local string
	char local_string_var[2] = "";
	snprintf(local_string_var, 2, "2");
	snprintf(local_string_var, sizeof(local_string_var), "2");

	// local anonymous structure variable
	struct {
		char string1[2];
		char string2[LENGTH3];
	} local_anon_struct_var;

	snprintf(local_anon_struct_var.string1, 2, "2");
	snprintf(local_anon_struct_var.string1, sizeof(local_anon_struct_var.string1), "2");
	snprintf(local_anon_struct_var.string2, 3, "33"); // no expectation
	snprintf(local_anon_struct_var.string2, LENGTH3, "33");
	snprintf(local_anon_struct_var.string2, sizeof(local_anon_struct_var.string2), "33");

	// local named structure variable
	struct named_struct local_named_struct_var;
	snprintf(local_named_struct_var.string1, 2, "2");
	snprintf(local_named_struct_var.string1, sizeof(local_named_struct_var.string1), "2");
	snprintf(local_named_struct_var.string2, 3, "33"); // no expectation
	snprintf(local_named_struct_var.string2, LENGTH3, "33");
	snprintf(local_named_struct_var.string2, sizeof(local_named_struct_var.string2), "33");

	// local named structure pointer
	struct named_struct *local_named_struct_ptr = &local_named_struct_var;
	snprintf(local_named_struct_ptr->string1, 2, "2");
	snprintf(local_named_struct_ptr->string1, sizeof(local_named_struct_ptr->string1), "2");
	snprintf(local_named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(local_named_struct_ptr->string2, LENGTH3, "33");
	snprintf(local_named_struct_ptr->string2, sizeof(local_named_struct_ptr->string2), "33");

	// local named struct type variable (with existing struct)
	named_struct_t local_named_struct_t_var;
	snprintf(local_named_struct_t_var.string1, 2, "2");
	snprintf(local_named_struct_t_var.string1, sizeof(local_named_struct_t_var.string1), "2");
	snprintf(local_named_struct_t_var.string2, 3, "33"); // no expectation
	snprintf(local_named_struct_t_var.string2, LENGTH3, "33");
	snprintf(local_named_struct_t_var.string2, sizeof(local_named_struct_t_var.string2), "33");

	// local named struct type pointer (with existing struct)
	named_struct_t *local_named_struct_t_ptr = &local_named_struct_t_var;
	snprintf(local_named_struct_t_ptr->string1, 2, "2");
	snprintf(local_named_struct_t_ptr->string1, sizeof(local_named_struct_t_ptr->string1), "2");
	snprintf(local_named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(local_named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(local_named_struct_t_ptr->string2, sizeof(local_named_struct_t_ptr->string2), "33");

	// local named struct type variable
	named_struct2_t local_named_struct2_t_var;
	snprintf(local_named_struct2_t_var.string1, 2, "2");
	snprintf(local_named_struct2_t_var.string1, sizeof(local_named_struct2_t_var.string1), "2");
	snprintf(local_named_struct2_t_var.string2, 3, "33"); // no expectation
	snprintf(local_named_struct2_t_var.string2, LENGTH3, "33");
	snprintf(local_named_struct2_t_var.string2, sizeof(local_named_struct2_t_var.string2), "33");

	// local named struct type pointer
	named_struct2_t *local_named_struct2_t_ptr = &local_named_struct2_t_var;
	snprintf(local_named_struct2_t_ptr->string1, 2, "2");
	snprintf(local_named_struct2_t_ptr->string1, sizeof(local_named_struct2_t_ptr->string1), "2");
	snprintf(local_named_struct2_t_ptr->string2, 3, "33"); // no expectation
	snprintf(local_named_struct2_t_ptr->string2, LENGTH3, "33");
	snprintf(local_named_struct2_t_ptr->string2, sizeof(local_named_struct2_t_ptr->string2), "33");

	// local anonymous struct type variable
	anon_struct_t local_anon_struct_t_var;
	snprintf(local_anon_struct_t_var.string1, 2, "2");
	snprintf(local_anon_struct_t_var.string1, sizeof(local_anon_struct_t_var.string1), "2");
	snprintf(local_anon_struct_t_var.string2, 3, "33"); // no expectation
	snprintf(local_anon_struct_t_var.string2, LENGTH3, "33");
	snprintf(local_anon_struct_t_var.string2, sizeof(local_anon_struct_t_var.string2), "33");

	// local anonymous struct type pointer
	anon_struct_t *local_anon_struct_t_ptr = &local_anon_struct_t_var;
	snprintf(local_anon_struct_t_ptr->string1, 2, "2");
	snprintf(local_anon_struct_t_ptr->string1, sizeof(local_anon_struct_t_ptr->string1), "2");
	snprintf(local_anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(local_anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(local_anon_struct_t_ptr->string2, sizeof(local_anon_struct_t_ptr->string2), "33");

	// local container variable
	struct container local_container_var;
	struct container *local_container_ptr = &local_container_var;

	struct named_struct *named_struct_ptr;

	named_struct_ptr = &local_container_var.named_member;

	snprintf(named_struct_ptr->string1, 2, "2");
	snprintf(named_struct_ptr->string1, sizeof(named_struct_ptr->string1), "2");
	snprintf(named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_ptr->string2, sizeof(named_struct_ptr->string2), "33");

	named_struct_ptr = &local_container_ptr->named_member;

	snprintf(named_struct_ptr->string1, 2, "2");
	snprintf(named_struct_ptr->string1, sizeof(named_struct_ptr->string1), "2");
	snprintf(named_struct_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_ptr->string2, sizeof(named_struct_ptr->string2), "33");

	named_struct_t *named_struct_t_ptr;

	named_struct_t_ptr = &local_container_var.named_t_member;

	snprintf(named_struct_t_ptr->string1, 2, "2");
	snprintf(named_struct_t_ptr->string1, sizeof(named_struct_t_ptr->string1), "2");
	snprintf(named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_t_ptr->string2, sizeof(named_struct_t_ptr->string2), "33");

	named_struct_t_ptr = &local_container_ptr->named_t_member;

	snprintf(named_struct_t_ptr->string1, 2, "2");
	snprintf(named_struct_t_ptr->string1, sizeof(named_struct_t_ptr->string1), "2");
	snprintf(named_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(named_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(named_struct_t_ptr->string2, sizeof(named_struct_t_ptr->string2), "33");

	anon_struct_t *anon_struct_t_ptr;

	anon_struct_t_ptr = &local_container_var.anon_t_member;

	snprintf(anon_struct_t_ptr->string1, 2, "2");
	snprintf(anon_struct_t_ptr->string1, sizeof(anon_struct_t_ptr->string1), "2");
	snprintf(anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(anon_struct_t_ptr->string2, sizeof(anon_struct_t_ptr->string2), "33");

	anon_struct_t_ptr = &local_container_ptr->anon_t_member;

	snprintf(anon_struct_t_ptr->string1, 2, "2");
	snprintf(anon_struct_t_ptr->string1, sizeof(anon_struct_t_ptr->string1), "2");
	snprintf(anon_struct_t_ptr->string2, 3, "33"); // no expectation
	snprintf(anon_struct_t_ptr->string2, LENGTH3, "33");
	snprintf(anon_struct_t_ptr->string2, sizeof(anon_struct_t_ptr->string2), "33");
}
