@@
expression DST;
@@
- snprintf(DST, sizeof(DST),
+ SNPRINTF(DST,
  ...);

// match an array declared as a variable
@as_variable@
identifier DST;
expression DSTSIZE;
@@
(
  char DST[DSTSIZE];
|
  char DST[DSTSIZE] = ...;
)

@@
identifier as_variable.DST;
expression as_variable.DSTSIZE;
@@
- snprintf(DST, DSTSIZE,
+ SNPRINTF(DST,
  ...);

// match a structure
@in_struct@
identifier STRUCT;
identifier DST;
expression DSTSIZE;
@@
  struct STRUCT {
      ...
      char DST[DSTSIZE];
      ...
  }

@@
identifier in_struct.STRUCT;
struct STRUCT VAR;
identifier in_struct.DST;
expression in_struct.DSTSIZE;
@@
- snprintf(VAR.DST, DSTSIZE,
+ SNPRINTF(VAR.DST,
  ...);

@@
identifier in_struct.STRUCT;
struct STRUCT *PTR;
identifier in_struct.DST;
expression in_struct.DSTSIZE;
@@
- snprintf(PTR->DST, DSTSIZE,
+ SNPRINTF(PTR->DST,
  ...);

// match a structure used to declare a new type
@as_typedef@
identifier in_struct.STRUCT;
type TYPE;
@@
  typedef struct STRUCT TYPE;

@@
type as_typedef.TYPE;
TYPE VAR;
identifier in_struct.DST;
expression in_struct.DSTSIZE;
@@
- snprintf(VAR.DST, DSTSIZE,
+ SNPRINTF(VAR.DST,
  ...);

@@
type as_typedef.TYPE;
TYPE *PTR;
identifier in_struct.DST;
expression in_struct.DSTSIZE;
@@
- snprintf(PTR->DST, DSTSIZE,
+ SNPRINTF(PTR->DST,
  ...);

// match an anonymous structure used to declare a new type
@in_anon_typedef@
identifier DST;
expression DSTSIZE;
type TYPE;
@@
  typedef struct
  {
  ...
  char DST[DSTSIZE];
  ...
  } TYPE;

@@
type in_anon_typedef.TYPE;
TYPE VAR;
identifier in_anon_typedef.DST;
expression in_anon_typedef.DSTSIZE;
@@
- snprintf(VAR.DST, DSTSIZE,
+ SNPRINTF(VAR.DST,
  ...);

@@
type in_anon_typedef.TYPE;
TYPE *PTR;
identifier in_anon_typedef.DST;
expression in_anon_typedef.DSTSIZE;
@@
- snprintf(PTR->DST, DSTSIZE,
+ SNPRINTF(PTR->DST,
  ...);

// match a named structure used to declare a new type
@in_typedef@
identifier STRUCT;
identifier DST;
expression DSTSIZE;
type TYPE;
@@
  typedef struct STRUCT
  {
  ...
  char DST[DSTSIZE];
  ...
  } TYPE;

@@
type in_typedef.TYPE;
TYPE VAR;
identifier in_typedef.DST;
expression in_typedef.DSTSIZE;
@@
- snprintf(VAR.DST, DSTSIZE,
+ SNPRINTF(VAR.DST,
  ...);

@@
type in_typedef.TYPE;
TYPE *PTR;
identifier in_typedef.DST;
expression in_typedef.DSTSIZE;
@@
- snprintf(PTR->DST, DSTSIZE,
+ SNPRINTF(PTR->DST,
  ...);



// match an anonymous structure used to declare a new type
@in_anon_var@
identifier DST;
expression DSTSIZE;
identifier VAR;
@@
  struct
  {
  ...
  char DST[DSTSIZE];
  ...
  } VAR;

@@
identifier in_anon_var.VAR;
identifier in_anon_var.DST;
expression in_anon_var.DSTSIZE;
@@
- snprintf(VAR.DST, DSTSIZE,
+ SNPRINTF(VAR.DST,
  ...);
